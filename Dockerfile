import os

import numpy as np
import tensorflow as tf

import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

assert hasattr(tf, 'function')  # To make sure we use tf2

my_dir = "dataset"
my_files = []
for file_name in os.listdir(my_dir):
    if ".npy" in file_name:
        my_files.append(file_name)

nb_max_per_class = 2000
my_classes = []

nb_drawings = 0
for file_name in my_files:
    my_drawings = np.load(os.path.join(my_dir, file_name))
    my_drawings = my_drawings[:nb_max_per_class]
    nb_drawings += my_drawings.shape[0]

my_images = np.zeros((nb_drawings, 28, 28))
my_targets = np.zeros((nb_drawings,))

it = 0
t = 0
for file_name in my_files:
    my_classes.append(file_name.replace("full_numpy_bitmap_", "").replace(".npy", ""))
    my_images[it:it+my_drawings.shape[0]] = np.invert(my_drawings.reshape(-1, 28, 28))
    my_targets[it:it+my_drawings.shape[0]] = t
    it += my_drawings.shape[0]
    t += 1

my_images = my_images.astype(np.float32)

shuffling_indexes = np.arange(nb_drawings)
np.random.shuffle(shuffling_indexes)
my_images = my_images[shuffling_indexes]
my_targets = my_targets[shuffling_indexes]
training_images, validation_images, training_targets, validation_targets = train_test_split(
    my_images,
    my_targets,
    test_size=0.40
)

print("training images shape : ", training_images.shape)
print("training targets shape : ", training_targets.shape)

print("validation images shape : ", validation_images.shape)
print("validation targets shape : ", validation_targets.shape)

print(my_classes)

my_scaler = StandardScaler()
scaled_training_images = my_scaler.fit_transform(training_images.reshape(-1, 28*28))
scaled_validation_images = my_scaler.transform(validation_images.reshape(-1, 28*28))

scaled_training_images = scaled_training_images.reshape(-1, 28, 28, 1)
scaled_validation_images = scaled_validation_images.reshape(-1, 28, 28, 1)

training_dataset = tf.data.Dataset.from_tensor_slices(scaled_training_images)
validation_dataset = tf.data.Dataset.from_tensor_slices(scaled_validation_images)


class MyCNN(tf.keras.Model):

    def __init__(self):
        super(MyCNN, self).__init__()
        self.conv1 = tf.keras.layers.Conv2D(32, 4, activation="relu", name="conv1")
        self.conv2 = tf.keras.layers.Conv2D(64, 3, activation="relu", name="conv2")
        self.conv3 = tf.keras.layers.Conv2D(218, 4, activation="relu", name="conv3")
        self.flatten = tf.keras.layers.Flatten(name="flatten")
        self.dense_layer = tf.keras.layers.Dense(128, activation="relu", name="dense_layer")
        self.output_layer = tf.keras.layers.Dense(10, activation="softmax", name="output_layer")

    def __call__(self, image):
        conv1 = self.conv1(image)
        conv2 = self.conv2(conv1)
        conv3 = self.conv3(conv2)
        flatten = self.flatten(conv3)
        dense_layer = self.dense_layer(flatten)
        output = self.output_layer(dense_layer)
        return output


my_model = MyCNN()
# my_model.predict(scaled_training_images[0:1])
my_loss = tf.keras.losses.SparseCategoricalCrossentropy()
my_optimizer = tf.keras.optimizers.Adam()

training_loss = tf.keras.metrics.Mean(name='training_loss')
validation_loss = tf.keras.metrics.Mean(name='validation_loss')

training_accuracy = tf.keras.metrics.SparseCategoricalAccuracy(name="training_accuracy")
validation_accuracy = tf.keras.metrics.SparseCategoricalAccuracy(name="validation_accuracy")


@tf.function
def training_step(images, targets):
    with tf.GradientTape() as tape:
        my_predictions = my_model(images)
        my_loss = my_loss(targets, predictions)
        gradients = tape.gradient(my_loss, my_model.trainable_variables)
        my_optimizer.apply_gradients(zip(gradients, my_model.trainable_variables))
        training_loss(my_loss)
        training_accuracy(targets, predictions)


@tf.function
def validation_step(images, targets):
    predictions = my_model(images)
    my_loss = my_loss(targets, predictions)
    validation_loss(my_loss)
    validation_accuracy(targets, predictions)


nb_epochs = 10
batch_size = 32
current_position = 0
for epoch in range(nb_epochs):
    for batch_images, batch_targets in training_dataset.batch(batch_size):
        training_step(images_batch, targets_batch)
        template = '\r Batch {}/{}, Loss: {}, Accuracy: {}'
        print(template.format(
            current_batch,
            len(my_targets),
            training_loss.result(),
            training_accuracy.result()*100
        ), end="")
    current_position += batch_size
    for batch_images, batch_targets in validation_dataset.batch(batch_size):
        validation_step(images_batch, targets_batch)

    template = 'Epoch {}, Validation Loss: {}, Validation Accuracy: {}'
    print(template.format(
        epoch+1,
        validation_loss.result(),
        validation_accuracy.result()*100
    ))

    validation_loss.reset_states()
    validation_accuracy.reset_states()
    training_loss.reset_states()
    training_accuracy.reset_states()
